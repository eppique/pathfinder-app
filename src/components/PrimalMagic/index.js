import React from "react";

class PrimalMagic extends React.Component {

  render() {
    const primalMagicReady = this.props.primalMagicReady;
    const generatePrimalMagic = this.props.generatePrimalMagic;
    const primalMagicList = this.props.primalMagicList;
    const primalMagic = this.props.primalMagic;

    var primalMagicDisplay;

    if (primalMagicReady) {
      primalMagicDisplay =
        <div>
          <table className="Active-page-table">
            <tbody>
              <tr className="UCT-header">
                <td><b>Primal Magic</b></td>
              </tr>
              <tr className="UCT-row">
                <td>{primalMagicList.primal[primalMagic].description}</td>
              </tr>
            </tbody>
        </table>
        </div>
    }
    else {
      primalMagicDisplay = 
      <div>
        <table className="Active-page-table">
            <tbody>
              <tr className="UCT-prompt">
                Press button for effect
              </tr>
              <tr className="UCT-header">
                <td><b>Primal Magic</b></td>
              </tr>
              <tr className="UCT-row">
                <td>???</td>
              </tr>
            </tbody>
          </table>
        </div>
    }

    return (
      <div className="PrimalMagic">
        <header className="Active-page-header">
          <p>
            Primal Magic Page
          </p>
        </header>
        <div>
          {primalMagicDisplay}
          <div>
            <button onClick={generatePrimalMagic}>
              Generate
            </button>
          </div>
        </div>
      </div>
    );
  }
}
export default PrimalMagic;