import React from "react";

class Shaman extends React.Component {

  render() {
    const shamanReady = this.props.shamanReady;
    const generateSpells = this.props.generateSpells;
    const shamanSpellList = this.props.shamanSpellList;
    const level1RS = this.props.level1RS;
    const level1AS = this.props.level1AS;
    const level2RS = this.props.level2RS;
    const level2AS = this.props.level2AS;
    const level3RS = this.props.level3RS;
    const level3AS = this.props.level3AS;
    const level4RS = this.props.level4RS;
    const level4AS = this.props.level4AS;
    const level5RS = this.props.level5RS;
    const level5AS = this.props.level5AS;
    const level6RS = this.props.level6RS;
    const level6AS = this.props.level6AS;
    const level7RS = this.props.level7RS;
    const level7AS = this.props.level7AS;
    const level8RS = this.props.level8RS;
    const level8AS = this.props.level8AS;
    const level9RS = this.props.level9RS;
    const level9AS = this.props.level9AS;

    var shamanDisplay;

    // <a
    //   className="App-link"
    //   href="https://reactjs.org"
    //   target="_blank"
    //   rel="noopener noreferrer"
    // >
    //   Learn React
    // </a>

    if (shamanReady) {
      shamanDisplay =
        <div>
          <table className="Active-page-table">
            <tbody>
              <tr className="UCT-header">
                <td>Spell Level</td>
                <td>Apparent Spell</td>
                <td>Real Spell</td>
              </tr>
              <tr className="UCT-row">
                <td>Level 1</td>
                <td><a className="Spell-link" target="_blank" rel="noopener noreferrer" href={shamanSpellList.level1[level1AS].link}>{shamanSpellList.level1[level1AS].name}</a></td>
                <td><a className="Spell-link" target="_blank" rel="noopener noreferrer" href={shamanSpellList.level1[level1RS].link}>{shamanSpellList.level1[level1RS].name}</a></td>
              </tr>
              <tr>
                <td> Level 2</td>
                <td><a className="Spell-link" target="_blank" rel="noopener noreferrer" href={shamanSpellList.level2[level2AS].link}>{shamanSpellList.level2[level2AS].name}</a></td>
                <td><a className="Spell-link" target="_blank" rel="noopener noreferrer" href={shamanSpellList.level2[level2RS].link}>{shamanSpellList.level2[level2RS].name}</a></td>
              </tr>
              <tr>
                <td> Level 3</td>
                <td><a className="Spell-link" target="_blank" rel="noopener noreferrer" href={shamanSpellList.level3[level3AS].link}>{shamanSpellList.level3[level3AS].name}</a></td>
                <td><a className="Spell-link" target="_blank" rel="noopener noreferrer" href={shamanSpellList.level3[level3RS].link}>{shamanSpellList.level3[level3RS].name}</a></td>
              </tr>
              <tr>
                <td> Level 4</td>
                <td><a className="Spell-link" target="_blank" rel="noopener noreferrer" href={shamanSpellList.level4[level4AS].link}>{shamanSpellList.level4[level4AS].name}</a></td>
                <td><a className="Spell-link" target="_blank" rel="noopener noreferrer" href={shamanSpellList.level4[level4RS].link}>{shamanSpellList.level4[level4RS].name}</a></td>
              </tr>
              <tr>
                <td> Level 5</td>
                <td><a className="Spell-link" target="_blank" rel="noopener noreferrer" href={shamanSpellList.level5[level5AS].link}>{shamanSpellList.level5[level5AS].name}</a></td>
                <td><a className="Spell-link" target="_blank" rel="noopener noreferrer" href={shamanSpellList.level5[level5RS].link}>{shamanSpellList.level5[level5RS].name}</a></td>
              </tr>
              <tr>
                <td> Level 6</td>
                <td><a className="Spell-link" target="_blank" rel="noopener noreferrer" href={shamanSpellList.level6[level6AS].link}>{shamanSpellList.level6[level6AS].name}</a></td>
                <td><a className="Spell-link" target="_blank" rel="noopener noreferrer" href={shamanSpellList.level6[level6RS].link}>{shamanSpellList.level6[level6RS].name}</a></td>
              </tr>
              <tr>
                <td> Level 7</td>
                <td><a className="Spell-link" target="_blank" rel="noopener noreferrer" href={shamanSpellList.level7[level7AS].link}>{shamanSpellList.level7[level7AS].name}</a></td>
                <td><a className="Spell-link" target="_blank" rel="noopener noreferrer" href={shamanSpellList.level7[level7RS].link}>{shamanSpellList.level7[level7RS].name}</a></td>
              </tr>
              <tr>
                <td> Level 8</td>
                <td><a className="Spell-link" target="_blank" rel="noopener noreferrer" href={shamanSpellList.level8[level8AS].link}>{shamanSpellList.level8[level8AS].name}</a></td>
                <td><a className="Spell-link" target="_blank" rel="noopener noreferrer" href={shamanSpellList.level8[level8RS].link}>{shamanSpellList.level8[level8RS].name}</a></td>
              </tr>
              <tr>
                <td> Level 9</td>
                <td><a className="Spell-link" target="_blank" rel="noopener noreferrer" href={shamanSpellList.level9[level9AS].link}>{shamanSpellList.level9[level9AS].name}</a></td>
                <td><a className="Spell-link" target="_blank" rel="noopener noreferrer" href={shamanSpellList.level9[level9RS].link}>{shamanSpellList.level9[level9RS].name}</a></td>
              </tr>
            </tbody>
          </table>
        </div>
    }
    else {
      shamanDisplay = <div>
        Press button for spells
        <table className="Active-page-table">
            <tbody>
              <tr className="UCT-header">
                <td>Spell Level</td>
                <td>Apparent Spell</td>
                <td>Real Spell</td>
              </tr>
              <tr className="UCT-row">
                <td>Level 1</td>
                <td>???</td>
                <td>???</td>
              </tr>
              <tr>
                <td> Level 2</td>
                <td>???</td>
                <td>???</td>
              </tr>
              <tr>
                <td> Level 3</td>
                <td>???</td>
                <td>???</td>
              </tr>
              <tr>
                <td> Level 4</td>
                <td>???</td>
                <td>???</td>
              </tr>
              <tr>
                <td> Level 5</td>
                <td>???</td>
                <td>???</td>
              </tr>
              <tr>
                <td> Level 6</td>
                <td>???</td>
                <td>???</td>
              </tr>
              <tr>
                <td> Level 7</td>
                <td>???</td>
                <td>???</td>
              </tr>
              <tr>
                <td> Level 8</td>
                <td>???</td>
                <td>???</td>
              </tr>
              <tr>
                <td> Level 9</td>
                <td>???</td>
                <td>???</td>
              </tr>
            </tbody>
          </table>
      </div>
    }

    return (
      <div className="Shaman">
        <header className="Active-page-header">
          <p>
            Shaman Page
          </p>
        </header>
        <div>
          {shamanDisplay}
          <div>
            <button onClick={generateSpells}>
              Generate
            </button>
          </div>
        </div>
      </div>
    );
  }
}
export default Shaman;