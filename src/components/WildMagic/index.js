import React from "react";

class WildMagic extends React.Component {

  render() {
    const wildMagicReady = this.props.wildMagicReady;
    const generateWildMagic = this.props.generateWildMagic;
    const wildMagicList = this.props.wildMagicList;
    const wildMagic = this.props.wildMagic;
    const universalMagic = this.props.universalMagic;

    var wildMagicDisplay;

    if (wildMagicReady) {
      wildMagicDisplay =
        <div>
          <table className="Active-page-table">
            <tbody>
              <tr className="UCT-header">
                <td><b>Wild Magic</b></td>
              </tr>
              <tr className="UCT-row">
                <td>{wildMagicList.wild[wildMagic].description}</td>
              </tr>
              <tr className="UCT-header">
                <td><b>If effect is unable to complete refer below:</b></td>
              </tr>
              <tr className="UCT-row">
                <td>{wildMagicList.universal[universalMagic].description}</td>
              </tr>
            </tbody>
          </table>
        </div>
    }
    else {
      wildMagicDisplay = 
      <div>
        <table className="Active-page-table">
            <tbody>
              <tr>
                Press button for effects
              </tr>
              <tr className="UCT-header">
                <td><b>Wild Magic</b></td>
              </tr>
              <tr className="UCT-row">
                <td>???</td>
              </tr>
              <tr className="UCT-header">
                <td><b>If effect is unable to complete refer below:</b></td>
              </tr>
              <tr className="UCT-row">
                <td>???</td>
              </tr>
            </tbody>
          </table>
        </div>
    }

    return (
      <div className="WildMagic">
        <header className="Active-page-header">
          <p>
            Wild Magic Page
          </p>
        </header>
        <div>
          {wildMagicDisplay}
          <div>
            <button onClick={generateWildMagic}>
              Generate
            </button>
          </div>
        </div>
      </div>
    );
  }
}
export default WildMagic;