import natureRotated from './natureRotated.svg';
import './App.css';
import Shaman from './components/Shaman';
import WildMagic from './components/WildMagic';
import PrimalMagic from './components/PrimalMagic';
import React from 'react';
import shamanSpellList from './shamanSpellList.json'
import wildMagicList from './wildMagicList.json'
import primalMagicList from './primalMagicList.json'



class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activePage: 0,
      shamanReady: 0,
      level1AS: 0,
      level1RS: 0,
      level2AS: 0,
      level2RS: 0,
      level3AS: 0,
      level3RS: 0,
      level4AS: 0,
      level4RS: 0,
      level5AS: 0,
      level5RS: 0,
      level6AS: 0,
      level6RS: 0,
      level7AS: 0,
      level7RS: 0,
      level8AS: 0,
      level8RS: 0,
      level9AS: 0,
      level9RS: 0,
      wildMagicReady: 0,
      wildMagic: 0,
      universalMagic: 0,
      primalMagicReady: 0,
      primalMagic: 0,
    }

    this.generateSpells = this.generateSpells.bind(this);
    this.generateWildMagic = this.generateWildMagic.bind(this);
    this.generatePrimalMagic = this.generatePrimalMagic.bind(this);
    this.selectShaman = this.selectShaman.bind(this);
    this.selectWildMagic = this.selectWildMagic.bind(this);
    this.selectPrimalMagic = this.selectPrimalMagic.bind(this);
    this.deselectActivePage = this.deselectActivePage.bind(this);
    this.render = this.render.bind(this);
  }

  componentDidMount() { }
  //componentWillUnmount() {}
  //componentDidUpdate() {}

  generateSpells() {
    const level1Max = shamanSpellList.level1.length;
    var realSpell;
    var apparentSpell;
    realSpell = (Math.floor(Math.random() * level1Max));
    apparentSpell = (Math.floor(Math.random() * level1Max));
    this.setState({ level1RS: realSpell }) //Real Spell
    if ((Math.floor(Math.random() * 2))) { // Apparent is different
      this.setState({ level1AS: apparentSpell })
    }
    else { // Apparent is the same
      this.setState({ level1AS: realSpell })
    }
    const level2Max = shamanSpellList.level2.length;
    realSpell = (Math.floor(Math.random() * level2Max));
    apparentSpell = (Math.floor(Math.random() * level2Max));
    this.setState({ level2RS: realSpell }) //Real Spell
    if ((Math.floor(Math.random() * 2))) { // Apparent is different
      this.setState({ level2AS: apparentSpell })
    }
    else { // Apparent is the same
      this.setState({ level2AS: realSpell })
    }
    const level3Max = shamanSpellList.level3.length;
    realSpell = (Math.floor(Math.random() * level3Max));
    apparentSpell = (Math.floor(Math.random() * level3Max));
    this.setState({ level3RS: realSpell }) //Real Spell
    if ((Math.floor(Math.random() * 2))) { // Apparent is different
      this.setState({ level3AS: apparentSpell })
    }
    else { // Apparent is the same
      this.setState({ level3AS: realSpell })
    }
    const level4Max = shamanSpellList.level4.length;
    realSpell = (Math.floor(Math.random() * level4Max));
    apparentSpell = (Math.floor(Math.random() * level4Max));
    this.setState({ level4RS: realSpell }) //Real Spell
    if ((Math.floor(Math.random() * 2))) { // Apparent is different
      this.setState({ level4AS: apparentSpell })
    }
    else { // Apparent is the same
      this.setState({ level4AS: realSpell })
    }
    const level5Max = shamanSpellList.level5.length;
    realSpell = (Math.floor(Math.random() * level5Max));
    apparentSpell = (Math.floor(Math.random() * level5Max));
    this.setState({ level5RS: realSpell }) //Real Spell
    if ((Math.floor(Math.random() * 2))) { // Apparent is different
      this.setState({ level5AS: apparentSpell })
    }
    else { // Apparent is the same
      this.setState({ level5AS: realSpell })
    }
    const level6Max = shamanSpellList.level6.length;
    realSpell = (Math.floor(Math.random() * level6Max));
    apparentSpell = (Math.floor(Math.random() * level6Max));
    this.setState({ level6RS: realSpell }) //Real Spell
    if ((Math.floor(Math.random() * 2))) { // Apparent is different
      this.setState({ level6AS: apparentSpell })
    }
    else { // Apparent is the same
      this.setState({ level6AS: realSpell })
    }
    const level7Max = shamanSpellList.level7.length;
    realSpell = (Math.floor(Math.random() * level7Max));
    apparentSpell = (Math.floor(Math.random() * level7Max));
    this.setState({ level7RS: realSpell }) //Real Spell
    if ((Math.floor(Math.random() * 2))) { // Apparent is different
      this.setState({ level7AS: apparentSpell })
    }
    else { // Apparent is the same
      this.setState({ level7AS: realSpell })
    }
    const level8Max = shamanSpellList.level8.length;
    realSpell = (Math.floor(Math.random() * level8Max));
    apparentSpell = (Math.floor(Math.random() * level8Max));
    this.setState({ level8RS: realSpell }) //Real Spell
    if ((Math.floor(Math.random() * 2))) { // Apparent is different
      this.setState({ level8AS: apparentSpell })
    }
    else { // Apparent is the same
      this.setState({ level8AS: realSpell })
    }
    const level9Max = shamanSpellList.level9.length;
    realSpell = (Math.floor(Math.random() * level9Max));
    apparentSpell = (Math.floor(Math.random() * level9Max));
    this.setState({ level9RS: realSpell }) //Real Spell
    if ((Math.floor(Math.random() * 2))) { // Apparent is different
      this.setState({ level9AS: apparentSpell })
    }
    else { // Apparent is the same
      this.setState({ level9AS: realSpell })
    }

    this.setState({ shamanReady: 1 })
  }

  generateWildMagic() {
    const wildMax = wildMagicList.wild.length;
    const uniMax = wildMagicList.universal.length;
    var wildMagicSelection = (Math.floor(Math.random() * wildMax));
    var universalMagicSelection = (Math.floor(Math.random() * uniMax));
    this.setState({ wildMagic: wildMagicSelection })
    this.setState({ universalMagic: universalMagicSelection })
    
    this.setState({ wildMagicReady: 1 })
  }

  generatePrimalMagic() {
    const primalMax = primalMagicList.primal.length;
    var primalMagicSelection = (Math.floor(Math.random() * primalMax));
    this.setState({ primalMagic: primalMagicSelection })
    
    this.setState({ primalMagicReady: 1 })
  }

  selectShaman() {
    this.setState({ activePage: 1 })
  }

  selectWildMagic() {
    this.setState({ activePage: 2 })
  }

  selectPrimalMagic() {
    this.setState({ activePage: 3 })
  }

  deselectActivePage() {
    this.setState({ activePage: 0 })
  }

  render() {
    var displayPage, logo;
    const activePage = this.state.activePage;

    switch (activePage){
      case 1:
        displayPage =
        <Shaman
          shamanReady={this.state.shamanReady}
          generateSpells={this.generateSpells}
          shamanSpellList={shamanSpellList}
          level1RS={this.state.level1RS}
          level1AS={this.state.level1AS}
          level2RS={this.state.level2RS}
          level2AS={this.state.level2AS}
          level3RS={this.state.level3RS}
          level3AS={this.state.level3AS}
          level4RS={this.state.level4RS}
          level4AS={this.state.level4AS}
          level5RS={this.state.level5RS}
          level5AS={this.state.level5AS}
          level6RS={this.state.level6RS}
          level6AS={this.state.level6AS}
          level7RS={this.state.level7RS}
          level7AS={this.state.level7AS}
          level8RS={this.state.level8RS}
          level8AS={this.state.level8AS}
          level9RS={this.state.level9RS}
          level9AS={this.state.level9AS}
        />
        logo = <div />
        break;
      case 2:
        displayPage =
        <WildMagic
          wildMagicReady={this.state.wildMagicReady}
          generateWildMagic={this.generateWildMagic}
          wildMagicList={wildMagicList}
          wildMagic={this.state.wildMagic}
          universalMagic={this.state.universalMagic}
        />
        logo = <div />
        break;
      case 3:
        displayPage =
        <PrimalMagic
          primalMagicReady={this.state.primalMagicReady}
          generatePrimalMagic={this.generatePrimalMagic}
          primalMagicList={primalMagicList}
          primalMagic={this.state.primalMagic}
        />
        logo = <div />
        break;
      default:
        displayPage = <div />
        logo = 
        <div className="App-logo-box-middle" id="logoBox">
          <img src={natureRotated} className="App-logo" alt="logo" id="appLogoID" />
        </div>
        break;
    }

    return (
      <div className="App" id="appID">
        <header className="App-header" id="appHeaderID">
            {logo}
          <div className="Main-page">
            <div className="Title">
              Main Page
            </div>
            <span>
              <button onClick={this.selectShaman} id="selectShamanButtonID">
                Shaman Page
              </button>
              <button onClick={this.selectWildMagic} id="selectWildMagicButtonID">
                Wild Magic Page
              </button>
              <button onClick={this.selectPrimalMagic} id="selectPrimalMagicButtonID">
                Primal Magic Page
              </button>
              <button onClick={this.deselectActivePage} id="deselectActivePageButtonID">
                Deselect Page
              </button>
            </span>
          </div>
          <div className="Active-page">
            {displayPage}
          </div>
        </header>
      </div>
    );
  }
}



export default App;
